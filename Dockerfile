FROM neo4j:3.0
MAINTAINER Sajjan Singh Mehta "sajjan.s.mehta@gmail.com"

RUN mkdir -p /data/databases
ADD includes/metaboxdb.tar.gz /data/databases/

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["neo4j"]
