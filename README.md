# MetaBoxDB Dockerfile

This repository contains a Dockerfile for MetaBoxDB, a Neo4j graph database with metabolome data to be used with [MetaBox](https://github.com/kwanjeeraw/metabox).

## Building

Build with the usual

    docker build -t metaboxdb .

and then run with

    docker run -d --publish=7474:7474 --publish=7687:7687 metaboxdb
